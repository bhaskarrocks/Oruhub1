import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  sidebar = false;
  date: any;
  userName: any;
  userInfo: any;
  menu: any;
  now = new Date();
  isMasterDataShow = false;
  isUserRoleShow = false;
  isUserShow = false;
  isBeaconsShow = false;
  isAdminShow = false;
  imgUrl = '';
  profilePicImage = '';
  isEmployeeUpload = false;
  isThemeSetup = false;
  isDisabledImg = true;

  constructor(private router: Router,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }
  toggleMenu(): void {
    // this.sidebar = !this.sidebar;
    // $('#page-container').removeClass('page-sidebar-toggled');
  }
  // calling logout api and clear session storage
  logout(): void {
    this.router.navigateByUrl('login');
  }
//
  activeRouteAddClass(ruoteringName): void {
    // $(document).ready( () => {
    //   $('#headerId').find('.header-class').removeClass('active');
    //   $('#adminId').removeClass('active');
    //   $('#' + ruoteringName).addClass('active');
    // });
  }
}
