import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.scss']
})
export class SetPasswordComponent implements OnInit {
  selectedPassword: any;
  selectedPassword2: any;
  pwdPattern = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&#])[A-Za-z\\d$@$!%*?&].{8,15}';
  selectedEmail: any;
  selectedCode: any;
  hide1 = true;
  hide2 = true;
  imgUrl = '';
  globalColor = '';
  bannerImage: any;
  // imgUrl = './assets/img/logo-login.png';
  constructor(private httpService: HttpClient, private activeRoute: ActivatedRoute,
              private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      // check type value and set in variables
      if (params.type !== undefined) {
      }
      // check code value and set in variables
      if (params.code !== undefined) {
        this.selectedCode = params.code;
      }
      // check email value and set in variables
      if (params.email !== undefined) {
        this.selectedEmail = params.email;
      }
    });
  }

  home(): void {
    this.router.navigateByUrl('/');
  }
  //
  verifySignup(): void {
  }
}
