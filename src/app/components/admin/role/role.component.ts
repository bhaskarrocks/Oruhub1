import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
export interface RoleData {
  screenId: number;
  screenName: string;
  SrNo: number;
  create: number;
  view: number;
  update: number;
  delete: number;
  approve: number;
}
@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  skip = 0;
  hide = true;
  create = 0;
  view = 0;
  update = 0;
  delete = 0;
  // approve = 0;
  // permissionArray = [];
  roleId: any;
  public dataSourceAddRole;
  /**
   * Creates an instance of form group, auto complete form control with data.
   */
  roleForm: FormGroup;
  displayedColumnsAddRole: string[] = ['SrNo', 'screenName', 'create', 'view', 'update', 'delete'];
  @ViewChildren('checkboxes') checkboxes: QueryList<ElementRef>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  
  public isBusy: boolean;
  public isBusySave: boolean;
  public useridControl: boolean;
  isRoleCreate: boolean;
  public headerText = 'Add Role';
  public buttonSave = 'Save';
  public btnsaveTooltip = 'Save';
  public btnreset = 'Clear the form';
  public btnClear = 'Cancel';
  type: any;
  readOnlyControls = false;
  public message: string;
  roleList: any = [];
  readOnlyUpdate = false;
  isTableHasData = false;
  viewAndUpdate = false;
  screenList = [];
  
  
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.roleForm = this.formBuilder.group({
      role: [''],
      description: ['']
    });
    this.checkLogin();
  }
  checkLogin() {
  }
  onRoleSubmit() {
  }
  showResponseStatus() {
  }
  bindDataTable() {
  }
}
