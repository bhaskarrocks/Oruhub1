import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
export interface UserListElement {
  SrNum: number;
  UserId: number;
  FirstName: string;
  Role: string;
  EmailId: string;
  Mobile: string;
  EmployeeId: number;
  Status: string;

}
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  formSearch: FormGroup;
  MatTableDataSource;

  public disable = false;
  statusOptions: any = [];
  roleOptions: any = [];

  constructor(private formBuilder: FormBuilder,public dialog: MatDialog) { }

  public userdataSource;
  userdisplayedColumns: string[] = ['SrNum', 'EmployeeId', 'FirstName', 'Mobile', 'EmailId', 'Role',  'Action'];
  @ViewChild(MatSort) sort: MatSort;
  skip = 0;
  pageFrom = this.skip + 1;
  pageTo: number;
  count = 5;
  isTableHasData = false;
  totalRecord = 1;
  lblSearch = 'Search';
  permissions: any;

  ngOnInit(): void {
    this.formSearch = this.formBuilder.group({
      email: [''],
      firstname: [''],
      number: [''],
      employeeId: [''],
      Status: [''],
      Role: [''],
    });
    this.checkLogin();
  }
  checkLogin(){    
  }
  public searchUser() {
  }
  nextPage() {}
  previousPage() {}
  bindDataTable(response) {
    if (response.numRows > 0) {
      const ELEMENT_DATA: UserListElement[] = [];
      // tslint:disable-next-line:prefer-for-of
      for (let j = 0; j < response.body.length; j++) {
        ELEMENT_DATA.push({
          SrNum: this.skip + j + 1,
          UserId: response.body[j].userId,
          FirstName: response.body[j].name,
          Role: response.body[j].role.role_name,
          EmailId: response.body[j].emailId,
          Mobile: response.body[j].mobile,
          Status: response.body[j].status,
          EmployeeId: response.body[j].employee_id
        });
      }
      if (this.userdataSource === undefined) {
        this.userdataSource = new MatTableDataSource(ELEMENT_DATA);
        this.userdataSource.sort = this.sort;
        this.isTableHasData = true;
      } else {
        this.userdataSource.data = ELEMENT_DATA;
        this.isTableHasData = true;
      }
    }
  }
}
