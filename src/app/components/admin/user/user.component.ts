import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  userForm: FormGroup;
  userId: any;
  public isBusyClear: boolean;
  public isBusySave: boolean;
  public buttonSave = 'Save';
  public btnsaveTooltip = 'Save';
  public btnreset = 'Clear the form';
  public btnClear = 'Cancel';
  type: any;
  readOnlyControls = false;
  noImage = false;
  public imagePath;
  public imageType: any;
  imgURL: any = './assets/img/no-image.png';
  roleList: any = [{ roleName: 'Admin', roleId: '1' },
  { roleName: 'System', roleId: '2' },
  { roleName: 'User', roleId: '3' }];
  public headerText = 'Add User';
  phoneMask = '000-000-0000';
  readOnlyStatus = true;
  name: string;
  ourFile: File; // hold our file
  readOnlyLocation = true;
  fileProgress: number;
  selectedRoleId: any;
  orgList: any = [{ orgName: 'Windpact LLC' },
  { orgName: 'RoadSafe LLC' },
  { orgName: 'Elemance' },
  { orgName: 'Ohio State University' },
  { orgName: 'North Carolina State University' }];
  departmentList: any = [{ name: 'Product Group' },
  { name: 'Administration' },
  { name: 'Operations' }];
  selectedUSAState: any;
  countrycodeData = [];
  selectedCunCode = 'USA';
  roleOptions: any = [];
  statusOptions: any = [];
  currentStatus = 'Inactive';
  currentOrg = 'Windpact LLC';
  USA_AddressList: any;
  selectedAddressId: any;
  selectedCompanyId: any;
  readOnlyUpdate = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      userId: [''],
      firstName: ['', Validators.required],
      mobileNo: ['', Validators.required],
      EmailId: ['', Validators.required],
      userRole: ['']
    });
    this.checkLogin();
  }
  checkLogin() {
    
  }
  checkEmployeeId() {
  }
  clearProfile() {
    this.imgURL = './assets/img/no-image.png';
    this.noImage = false;
    this.imagePath = undefined;
  }
  _keyPress(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
    
  }
  resetForm() {
  }
  preview(files) {
  }
  onUserSubmit() {
  }

}
