import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
export interface RoleListElement {
  SrNo: number;
  role: string;
  description: string;
  RoleId: string;

}
@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit {

  formSearch: FormGroup;
  public disable = false;
  roleOptions = [];
  description: any;
  public roledataSource;
  roledisplayedColumns: string[] = ['SrNo', 'RoleName', 'description', 'action'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedRole = '';
  isTableHasData = false;
  permissions: any;
  skip = 0;
  pageFrom = this.skip + 1;
  pageTo: number;
  count = 5;
  totalRecord = 1;

  constructor( public dialog: MatDialog,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.formSearch = this.formBuilder.group({
      RoleName: [''],
      description : ['']
    });
    this.checkLogin();
  }
  checkLogin() {

  }
  previousPage() {
  }
  nextPage() {
  }
  asyncGetFilterRoleData() {

  }
}