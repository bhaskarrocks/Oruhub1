import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  currentPassword: any;
  selectedPassword: any;
  selectedConfirmPassword: any;
  hide = true;
  hide1 = true;
  menu: any;

  constructor(private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit(): void {
  }
// change password click event
  changePassword(): void {
  }
  // redirect to the home page
  home(): void {
    this.router.navigateByUrl('/');
  }
}
