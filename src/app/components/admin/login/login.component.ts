import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  imgUrl: any;
  selectedEmail: any;
  password: any;
  constructor(private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }
  //
  home(): void {

  }
  //
  forgotPassword(): void {
    this.router.navigateByUrl('forgot-password');
  }
  login(): void {

  }
}
