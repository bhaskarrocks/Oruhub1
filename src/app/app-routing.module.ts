import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/admin/login/login.component';
import {ForgotPasswordComponent} from './components/admin/forgot-password/forgot-password.component';
import {ChangePasswordComponent} from './components/admin/change-password/change-password.component';
import {SetPasswordComponent} from './components/admin/set-password/set-password.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: 'change-password', component: ChangePasswordComponent},
  {path: 'user-verification/signup/:email/:code', component: SetPasswordComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule { }
