import { Component } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {NavigationStart, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'oruhub';
  showHead = false;

  constructor(private http: HttpClient, private router: Router, private spinner: NgxSpinnerService) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event.url === '/login' || event.url === '/' || event.url === '/forgot-password' || event.url === '/thank-you'
          || event.url === '/password' || event.url === '/set-password' || event.url === '/reset-password'
          || event.url.includes('/mobile-user-verification') || event.url.includes('/user-verification')) {
          this.showHead = false;
        } else {
          this.showHead = true;
        }
      }
    });
  }
}
